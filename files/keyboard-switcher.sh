#!/bin/sh

# Toggle between two different keyboard layouts, and update the
# scroll-lock led to indicate current status.
# Usage: <script> <layout-when-led-is-off> <layout-when-led-is-on>

layout_off="$1"
layout_on="$2"

result=$(xset q | grep 'Scroll Lock: off')
if [ -z "$result" ]; then
	setxkbmap $layout_off
	xset -led named "Scroll Lock"
else
	setxkbmap $layout_on
	xset led named "Scroll Lock"
fi
