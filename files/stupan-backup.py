#!/usr/bin/env python3

import os
import os.path
import subprocess
from datetime import datetime

def load_lines(path):
    with open(os.path.expanduser(path)) as f:
        lines = []
        for line in f:
            line = line.strip()
            if not line: continue
            if line[0] == '#': continue
            lines.append(os.path.expanduser(line))
        return lines


def main():
    files = load_lines("~/.config/backup/files")
    dests = load_lines("~/.config/backup/dests")

    dry_run = bool(os.getenv('DRY_RUN'))

    for file in files:
        filename, extension = os.path.splitext(file)
        filename = os.path.basename(filename)
        destname = f'{filename}-{datetime.now():%Y-%m-%d}{extension}'
        for dest in dests:
            # NOTE ensuring that the target directory exists would be nice,
            # but I see no way of checking if the destination is a local
            # or remote directory.
            # For local directories:
            # attempt mkdir
            # if it fails due to file existing:
            #     if it's already a directory:
            #         do nothing quietly
            #     else:
            #         emit an error, and continue
            # else:
            #     emit an error, and continue
            full_dest = os.path.join(dest, destname)
            cmdline = ['rsync', '-s', file, full_dest]
            if dry_run:
                print(cmdline)
            else:
                subprocess.run(cmdline)

if __name__ == '__main__':
    main()
