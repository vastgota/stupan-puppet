.PHONY: all install run

SOURCE_DIRS = files manifests

all:
	@echo Installs and runs a puppet setup for Stupandatorn
	@echo
	@echo All of these commands have sudo built-in to them
	@echo
	@echo make install_deps - installs pre-requisites
	@echo make install - installs these modules to the global puppet dir
	@echo make run - applies site.pp
	@echo

install_deps:
	sudo apt install rsync puppet

install:
	sudo rsync --delete -a $(SOURCE_DIRS) $(DESTDIR)/etc/puppet/code/modules/stupan/
	sudo mkdir -p /usr/local/lib/site_ruby/facter
	sudo cp -a lib/facter/like_ubuntu.rb /usr/local/lib/site_ruby/facter/like_ubuntu.rb

run:
	sudo puppet apply --test site.pp
