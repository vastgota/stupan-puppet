Facter.add('like_ubuntu') do
  confine 'os' do |os|
    os['distro']['id'] == 'Linuxmint'
  end
  setcode do
    /^UBUNTU_CODENAME=(.*)/.match(File.open('/etc/os-release').read)[1]
  end 
end
