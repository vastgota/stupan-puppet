class stupan::profiles::vgn {
  group { 'vgn':
  }

  user { 'vastgota':
    groups     => ['vgn'],
    membership => 'minimum',
  }
}
