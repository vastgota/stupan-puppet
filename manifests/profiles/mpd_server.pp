class stupan::profiles::mpd_server (
  String $user = 'vastgota',
) {
  mpd::user_instance { $user:
    mpd_conf => {
      music_directory => "/home/${user}/Musik",
      auto_update => false,
      max_output_buffer_size => 32768,
    }
  }

  mpd::output { 'Stupan pulseaudio output':
    type => 'pulse',
    target => '/home/vastgota/.config/mpd/mpd.conf',
  }

  # Disable systemd mpd to discourage accidental start from someone attempting
  # to start the wrong service.
  file { '/etc/systemd/system/mpd.service':
    ensure => 'link',
    target => '/dev/null',
  }

  ensure_packages([
    'wildmidi', # for MIDI playback
    'timidity', # Apparently needed with how MPD uses wildmidi
  ])
}
