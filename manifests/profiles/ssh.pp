class stupan::profiles::ssh {
  ensure_packages ([
    'openssh-client',
    'openssh-server',
  ])

  service { 'sshd':
    ensure => running,
    enable => true,
  }
}
