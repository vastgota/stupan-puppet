class stupan::profiles::backup (
  String $user = 'weibot',
  String $group = $user,
  String $home = "/home/${user}",
) {

  file { '/usr/lib/stupan-backup':
    source => 'puppet:///modules/stupan/stupan-backup.py',
    mode   => '0755',
  }

  file { default:
    owner   => $user,
    group   => $group,
    replace => false,
    ;
  "${home}/.config/":
    ensure => directory,
    ;
  "${home}/.config/backup":
    ensure => directory,
    ;
  "${home}/.config/backup/files":
    source => 'puppet:///modules/stupan/backup-files',
    ;
  "${home}/.config/backup/dests":
    source => 'puppet:///modules/stupan/backup-dests',
    ;
  }

  cron { 'Backup Stupan Files':
    command => '/usr/lib/stupan-backup',
    user    => $user,
    hour    => 12,
    minute  => 0,
  }

  include ::stupan::profiles::vgn

  user { $user:
    system     => true,
    groups     => ['vgn'],
    membership => 'minimum',
    managehome => true,
  }
}
