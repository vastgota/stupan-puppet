# Configures Systemd Networkd on the system.
#
# Linux Mint seems to use all network manegement systems at once, and
# none of the seems to work, so we might as well uses systemd networkd
# on top of everything.
class stupan::profiles::systemd_networkd {
  service { 'systemd-networkd':
    ensure => running,
    enable => true,
  }
}
