class stupan::profiles::common {
  $user = 'vastgota'
  # or $facts['os']['distro']['codename'], if $facts['os']['distro']['id'] == Ubuntu
  $ubuntu_codename = $facts['like_ubuntu']
  stupan::repo { 'adrift-space':
    url => "https://repo.adrift.space/debian ${ubuntu_codename} main",
  }

  ensure_packages([
    'tree',
    'vim-gtk3',
    'vim-puppet',
  ])

  ### Hiera setup ###
  # Technically not needed since hiera isn't used, but the version shiped with
  # Mint 21.1 is hiera 3 formated, which causes puppet to emit a warning every
  # run.
  file { '/etc/puppet/hiera.yaml':
    source => 'puppet:///modules/stupan/hiera.yaml',
  }
}
