# @summary Configures a node as an mpd client
#
# Note that cantata still has to be configured manually.
# It could be configured automatically, but its configuration
# is both for configuration and state (classic Qt), which means
# that a simple file resource is insufficient, and I don't want
# to depend on puppetlabs-inifile
class stupan::profiles::mpd_client (
  String $mpd_host = 'bragi.vastgota.nation.liu.se',
) {
  ensure_packages([
    'mpc',
    'ncmpc',
    'ncmpcpp',
    'cantata',
  ])

  file { '/etc/environment.d/10-mpd.conf':
    content => "MPD_HOST=${mpd_host}\n",
  }
}

