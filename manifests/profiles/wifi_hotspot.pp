# Configures machine as WiFi Hotspot
#
# Configures the given WiFi interface as a hotspot, but don't bridge it
# to the internet.
#
# Also configures a firewall which drops
# basically everything on the public facing
# interface, and accept everything on the WiFi
# hotspot interface.
#
# @param iface
#   Interface to run the hotspot on.
# @param internet_iface
#   Interface connected to the internet. Currently only used to configure firewall.
# @param ssid
#   SSID of interface.
# @param passphrase
#   Passphrase of interface.
class stupan::profiles::wifi_hotspot (
  String $iface = 'wlp2s0',
  String $internet_iface = 'enp3s0',
  String $ssid = 'Vastgota Admin',
  String $passphrase = 'truttrut',
) {
  ensure_packages([
    'hostapd',
    'isc-dhcp-server',
  ])

  file { "/etc/hostapd/${iface}.conf":
    notify  => Service["hostapd@${iface}"],
    content => @("EOF")
interface=${iface}
# bridge=br0

driver=nl80211

country_code=SE

ssid=${ssid}
hw_mode=g
channel=1

macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=${passphrase}
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
    | EOF
  }

  service { "hostapd@${iface}":
    ensure => running,
    enable => true,
  }

  file { '/etc/dhcp/dhcpd.conf':
    notify  => Service['isc-dhcp-server'],
    content => @("EOF")
    option domain-name "vastgota.nation.liu.se";
    # option domain-name-servers ns1.example.org;
    default-lease-time 600;
    max-lease-time 7200;

    ddns-update-style none;

    subnet 10.30.30.0 netmask 255.255.255.0 {
        range 10.30.30.10 10.30.30.20;
    }
    | EOF
  }

  file { '/etc/default/isc-dhcp-server':
    notify => Service['isc-dhcp-server'],
    content => @("EOF")
    # DHCPDv4_CONF=/etc/dhcp/dhcpd.conf
    # DHCPDv4_PID=/var/run/dhcpd.pid
    INTERFACESv4="${iface}"
    | EOF
  }

  service { 'isc-dhcp-server':
    enable => true,
    ensure => running,
  }


  service { 'nftables':
    enable => true,
  }

  file { '/etc/nftables.conf':
    notify  => Service['nftables'],
    content => @("EOF")
      #!/usr/sbin/nft -f
      
      flush ruleset
      
      table inet filter {
      	chain input {
      		type filter hook input priority 0;
      		iif lo accept
      		iifname ${iface} accept
      		iifname ${internet_iface} jump public_chain
      	}
      	chain public_chain {
      		ct state {established,related} accept
      		ct state invalid drop
      		reject with icmpx port-unreachable comment "all other traffic"
      	}
      	chain forward {
      		type filter hook forward priority 0;
      	}
      	chain output {
      		type filter hook output priority 0;
      	}
      }
      | EOF
  }

  include ::stupan::profiles::systemd_networkd

  file { '/etc/systemd/network/20-wlp2s0.network':
    notify  => Service['systemd-networkd'],
    content => @("EOF")
    [Match]
    Name=${iface}
    
    [Network]
    Address=10.30.30.1/24
    | EOF
  }
}
