class stupan::profiles::ashuffle (
  String $user = 'vastgota',
  String $group = 'vastgota',
  String $home = "/home/${user}",
  String $config_home = "${home}/.config",
  String $systemd_user_dir = "${home}/.config/systemd/user",
) {
  # TODO ensure_packages(['ashuffle'])

  file { '/home/vastgota/.config/systemd/user/ashuffle.service':
    ensure => file,
    source => "puppet:///modules/${module_name}/ashuffle.service",
    owner  => $user,
    group  => $group,
  }

  file {
    default:
      owner  => $user,
      group  => $group,
      ;
    "${systemd_user_dir}/mpd.service.wants":
      ensure => directory ;
    "${systemd_user_dir}/mpd.service.wants/ashuffle.service":
      ensure => link,
      target => "${systemd_user_dir}/ashuffle.service" ;
  }

  exec { "Enable user ${user} ashuffle instance":
    command => "systemctl enable --machine=${user}@.host --now --user ashuffle",
    creates => "${config_home}/systemd/user/default.target.wants/ashuffle.service",
    path    => ['/bin', '/usr/bin'],
  }
}
