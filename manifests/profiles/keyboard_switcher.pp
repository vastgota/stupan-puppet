# @summary Toggle between keyboard layouts with scroll-lock
#
# Changes scroll-lock to toggle between two keyboard layouts.
#
# This module is xfce4 specific, but the general idea can be applied
# to any environment which can bind scroll lock. (The accompanying
# script is DE agnostic, only requiring `setxkbmap` and `xset` to work)
#
# The setting parameters `user`, `off_layout`, and `on_layout` are
# probably the only ones you want to change.
#
# @param script_path
#   Location the script should be installed to
# @param user
#   User to apply this for. Needed since xfce4 configures settings per
#   user
# @param home
#   Home directory for `$user`.
# @param conf_file
#   Path to the file containing xfce4's custom keybindings.
# @param off_layout
#   Layout to use when scroll-lock is off.
# @param on_layout
#   Layout to use when scroll-lock is on.
class stupan::profiles::keyboard_switcher (
  String $script_path = '/usr/lib/keyboard-switcher.sh',
  String $user = 'vastgota',
  String $home = "/home/${user}",
  String $conf_file = "${home}/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml",
  String $off_layout = 'se',
  String $on_layout = 'se dvorak_a5',
) {
  file { $script_path:
    source => 'puppet:///modules/stupan/keyboard-switcher.sh',
    mode   => '0755',
  }

  $location = [
    'channel',
    'property[#attribute/name="commands"]',
    'property[#attribute/name="custom"]',
    'property[#attribute/name="Scroll_Lock"]',
  ].join('/')

  $cmdline = [$script_path, "'${off_layout}'", "'${on_layout}'"]
    .join(' ')

  # We both update the file, and manually set it.
  # Xfsettingsd doesn't like anyone else using editing its files, and
  # will only reload if modified by approved means (xfconf-query,
  # through the GUI). It will also usually save it's current state to
  # the files on logout. I'm not however certain it does that
  # correctly, so we also do it manually. This also allows puppet to
  # check the file, instead of having to run xfconf-query a second
  # (first) time to get the current state.
  #
  # TODO this still doesn't update until re-login. And it runs every time due
  # to some weird quoting rules in xfconf-query...
  augeas { 'Setup scroll-lock keyboard swither':
    incl    => $conf_file,
    lens    => 'Xml.lns',
    changes => [
      # This also creates the node if it doesn't exist yet
      "set ${location}/#attribute/name Scroll_Lock",
      "set ${location}/#attribute/type string",
      "set ${location}/#attribute/value \"${cmdline}\"",
    ],
  } ~>
  exec { 'xfconf-query Scroll_Lock':
    # Remove shellquote once updated from puppet 5 to something which
    # takes a command line list.
    command     => shellquote([
      'xfconf-query',
      '--channel',  'xfce4-keyboard-shortcuts',
      '--property', '/commands/custom/Scroll_Lock',
      '--create',
      '--type', 'string',
      '--set', $cmdline,
    ]),
    user        => $user,
    path        => ['/bin', '/usr/bin'],
    refreshonly => true,
  }
}
