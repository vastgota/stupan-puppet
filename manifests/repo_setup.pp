class stupan::repo_setup (
  String $sources_path = '/etc/apt/sources.list.d',
) {
  exec { 'apt update':
    refreshonly => true,
    provider    => 'shell',
  }
}
