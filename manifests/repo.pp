define stupan::repo (
  String $filename = $name,
  String $url,
) {
  include stupan::repo_setup
  file { "${stupan::repo_setup::sources_path}/${filename}.list":
    notify  => Exec['apt update'],
    content => @("EOF")
    deb [allow-insecure=yes] ${url}
    deb-src [allow-insecure=yes] ${url}
    | EOF
  }
}
