node njord.vastgota.nation.liu.se {
  include stupan::profiles::common
  include stupan::profiles::mpd_client
}

node bragi.vastgota.nation.liu.se {
  include stupan::profiles::common
  include stupan::profiles::kassa
  include stupan::profiles::backup
  include stupan::profiles::mpd_server
  include stupan::profiles::mpd_client
  include stupan::profiles::ashuffle
  include stupan::profiles::ssh
  include stupan::profiles::keyboard_switcher
  # xfconf-query --channel thunar --property /misc-exec-shell-scripts-by-default --create --type bool --set true
  # package: scrot
  include stupan::profiles::wifi_hotspot
}
